import os
from io import StringIO, BytesIO
import csv

import pandas as pd
import requests
from telegram import Location

kg_groups_table = [

]  # номер днз, група, вікові межі, всього місць, людей в черзі, рік

kg_table = {}


def update_groups_table(*, path=None,
                        xlsx_url=None,
                        csv_url=None):
    new_table = get_new_groups_table(path=path, xlsx_url=xlsx_url, csv_url=csv_url)
    new_table = format_new_groups_table(new_table)
    global kg_groups_table
    kg_groups_table = new_table


def format_new_groups_table(new_table):
    new_table = new_table[1:]
    for row in new_table:
        if empty_row(row):
            continue
        group = row[1]
        group = group.lower().strip()
        row[1] = group
        row[0] = int(row[0])
        row[3] = int(row[3])
        row[4] = int(row[4])
    return tuple(tuple(row[:5]) for row in new_table)


def empty_row(row):
    return row[0].strip() == ""


def update_kg_table():
    new_table = get_new_kg_table()
    global kg_table
    kg_table = new_table


def get_new_kg_table():
    script_dir = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(script_dir, 'kg_data.csv')
    file = open(path, encoding='utf-8')
    reader = csv.DictReader(file)
    table = {}
    for row in reader:
        location = Location(row['longitude'], row['latitude'])
        row['location'] = location
        kg_id = int(row['dnz_id'])
        table[kg_id] = row
    return table


def get_new_groups_table(*, path=None,
                         xlsx_url=None,
                         csv_url=None):
    if path:
        file = open(path)
    elif csv_url:
        resp = requests.get(csv_url)
        file = StringIO(resp.text)
    else:
        resp = requests.get(xlsx_url)
        data_frame = pd.read_excel(BytesIO(resp.content))
        file = StringIO(data_frame.to_csv(index=False, float_format="%.0f"))
    reader = csv.reader(file)
    return list(reader)


def get_group_catalog(group):
    return sorted(
        filter(lambda row: row[1] == group, kg_groups_table),
        key=lambda row: row[3] - row[4],
        reverse=True
    )


def get_single_kg_info(kg_number):
    groups = filter(lambda r: r[0] == kg_number, kg_groups_table)
    info = kg_table[kg_number]
    return info, groups


def get_kg(dnz_id):
    return kg_table[dnz_id]
