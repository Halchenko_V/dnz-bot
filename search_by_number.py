from telegram import ParseMode


def make_kg_info_message(number, info: dict, groups: list):
    text = get_message_text(number, info, groups)
    return {
        'text': text,
        'parse_mode': ParseMode.HTML,
    }


def get_message_text(number, info, groups):
    def records():
        for _, group, age, total_places, in_queue in groups:
            yield f"  Група: {group}\n" \
                f"  Всього місць: {total_places}\n" \
                f"  У черзі: {in_queue}\n" \
                f"  Вікові рамки: {age}\n"

    groups_info = '\n'.join(records())
    adress = info['adress']
    return f'ДНЗ №{number}\n' \
        f'Адреса: {adress}\n' \
        f'\n\n{groups_info}'
