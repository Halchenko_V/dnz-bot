from functools import partial

from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from utils import get_page, get_total_pages


EMPTY = 'empty'


def make_group_catalog_message(group_data, group_name, page, ipp=10):
    markup = get_kg_group_markup(group_data, page, group_name, ipp)
    data = get_page(group_data, page, ipp)
    text = get_kg_group_text(data, group_name)
    return {
        'text': text,
        'reply_markup': markup,
    }


def get_kg_group_markup(data, page, group_name, ipp):
    total_pages = get_total_pages(len(data), ipp)
    spc = partial(search_pagination_code, page, total_pages, group_name)
    kb = [
        [
            InlineKeyboardButton(text="1",
                                 callback_data=spc(1)),

            InlineKeyboardButton(text="<",
                                 callback_data=spc(page - 1)),

            InlineKeyboardButton(text=str(page), callback_data=EMPTY),

            InlineKeyboardButton(text=">",
                                 callback_data=spc(page + 1)),

            InlineKeyboardButton(text=str(total_pages),
                                 callback_data=spc(total_pages))
        ]
    ]

    return InlineKeyboardMarkup(kb)


def get_kg_group_text(data, group):
    def records():
        for dnz_number, _, age, total_places, in_queue in data:
            yield f"    ДНЗ №{dnz_number}\n" \
                f"   Всього місць: {total_places}\n" \
                f"   У черзі: {in_queue}\n" \
                f"   Вікові рамки: {age}\n"

    groups_text = '\n\n'.join(records())
    header = f'Група: {group}'
    return f'{header}\n{groups_text}'


def search_pagination_code(current_page, total_pages, group_name, page):
    page = max(1, page)
    page = min(total_pages, page)
    if page == current_page:
        return EMPTY
    return f'{group_name}-{page}'
