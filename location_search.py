import geopy.distance

from data import get_group_catalog, get_kg


def get_closest_groups_message(location, group):
    closest = get_closest_groups(location, group)
    text = get_kg_group_text(closest)
    return {
        'text': text
    }


def get_kg_group_text(data):
    def records():
        for dnz_number, _, age, total_places, in_queue, dist in data:
            yield f"   ДНЗ №{dnz_number}\n" \
                f"   Всього місць: {total_places}\n" \
                f"   У черзі: {in_queue}\n" \
                f"   Вікові рамки: {age}\n" \
                f"   Відстань: {dist}м"

    text = '\n\n'.join(records())
    return text


def get_closest_groups(location, group):
    def dst(row):
        dnz_id = row[0]
        dnz_id = int(dnz_id)
        dnz_location = get_kg(dnz_id)['location']
        dist = distance(location, dnz_location)
        return dist

    groups = [(*row, dst(row)) for row in get_group_catalog(group)]
    return sorted(groups, key=lambda row: row[-1])[:10]


def distance(location1, location2):
    lat1, lng1 = location1.latitude, location1.longitude
    lat2, lng2 = location2.latitude, location2.longitude
    d = geopy.distance.distance((lat1, lng1),
                                (lat2, lng2))
    d = d.meters
    return int(d)

