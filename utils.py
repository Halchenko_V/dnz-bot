
def get_page(group_data, page, ipp):
    start = (page - 1) * ipp
    end = start + ipp
    return group_data[start:end]


def get_total_pages(size, ipp):
    total_pages = size // ipp
    if size % ipp:
        total_pages += 1
    return total_pages
