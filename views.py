import logging

from telegram.ext import CommandHandler, MessageHandler, \
    CallbackQueryHandler, Filters, ConversationHandler

from telegram import ReplyKeyboardMarkup

from group_search import make_group_catalog_message, EMPTY
from data import get_group_catalog, get_single_kg_info, get_kg
from search_by_number import make_kg_info_message
from location_search import get_closest_groups_message

logger = logging.getLogger(__name__)


def start(update, context):
    update.message.reply_text((
        "Привіт! Я бот на основі відкритих даних Луцька.\n"
        "Я надаю інформацію про найближчі і найвільніші дитячі "
        "садки, кількість місць у них.\n\n"

        "Відкриті дані Луцька: https://www.lutskrada.gov.ua/pages/open-data\n"
        "ДНЗ Луцька: http://old.lutskrada.gov.ua/all-child-request\n\n"
        "Наступні команди відсилають інформацію по вільних місцях у відповідній категорії груп\n"
        "/yasla\n"
        "/molodsha\n"
        "/serednia\n"
        "/starsha\n\n"
        "Усі ці команди є у меню команд.\n"
        "Ви також можете просто надіслати в чат номер ДНЗ та отримати інформацію по ньому"
    ))


CHOOSING_GROUP = 23


def enter_geosearch(update, context):
    location = update.message.location
    context.user_data['location'] = location

    kb = [
        ["Ясельна", "Молодша"],
        ["Середня", "Старша"],
    ]
    kb = ReplyKeyboardMarkup(kb, one_time_keyboard=True)
    update.message.reply_text('Оберіть групу',
                              reply_markup=kb)

    return CHOOSING_GROUP


def geosearch(update, context):
    location = context.user_data['location']
    group_name = update.message.text.lower()
    message = get_closest_groups_message(location, group_name)
    update.message.reply_text(**message)
    return -1


def search(update, context):
    txt = update.message.text
    group = txt[1:]  # removing "/" symbol
    group_data = get_group_catalog(resolve_group(group))
    message = make_group_catalog_message(group_data, group, 1)
    update.message.reply_text(**message)


def search_paginate(update, context):
    cbd = update.callback_query.data
    group, page = get_group_and_page(cbd)
    group_data = get_group_catalog(resolve_group(group))
    message = make_group_catalog_message(group_data, group, page)
    update.callback_query.edit_message_text(**message)


def resolve_group(group):
    if group == 'yasla':
        return "ясельна"
    elif group == 'molodsha':
        return "молодша"
    elif group == 'starsha':
        return 'старша'
    elif group == 'serednia':
        return 'середня'
    else:
        raise ValueError('Bad group name value: %s' % group)


def get_group_and_page(cbd):
    group, page = cbd.split('-')
    return group, int(page)


def info_by_kg_number(update, context):
    number = update.message.text
    number = int(number)
    info, groups = get_single_kg_info(number)
    message = make_kg_info_message(number, info, groups)
    update.message.reply_text(**message)
    update.message.reply_location(quote=True,
                                  location=get_kg(number)['location'])


def help(update, context):
    update.message.reply_text((
        "Відкриті дані Луцька: https://www.lutskrada.gov.ua/pages/open-data\n"
        "ДНЗ Луцька: http://old.lutskrada.gov.ua/all-child-request\n\n"
        "Наступні команду відсилають інформацію по вільних місцях у відповідній категорії груп\n"
        "/yasla\n"
        "/molodsha\n"
        "/serednia\n"
        "/starsha\n\n"
        "Усі ці команди є у меню команд.\n"
        "Ви також можете просто надіслати в чат номер ДНЗ та отримати інформацію по ньому"
    ))


def empty_handler(update, context):
    update.callback_query.answer()


conv_handler = ConversationHandler(
    entry_points=[
        MessageHandler(Filters.location,
                       callback=enter_geosearch,
                       pass_user_data=True)
    ],

    states={
        CHOOSING_GROUP: [MessageHandler(Filters.text,
                                        callback=geosearch,
                                        pass_user_data=True)]
    },
    fallbacks=[]
)


handlers = [
    conv_handler,
    MessageHandler(Filters.text, info_by_kg_number),

    CommandHandler('start', callback=start),
    CommandHandler('help', callback=help),

    CommandHandler('yasla', callback=search),
    CommandHandler('molodsha', callback=search),
    CommandHandler('starsha', callback=search),
    CommandHandler('serednia', callback=search),

    CallbackQueryHandler(pattern=r'^\w+-\d+$',
                         callback=search_paginate),
    CallbackQueryHandler(pattern=f'^{EMPTY}$',
                         callback=empty_handler),
]

