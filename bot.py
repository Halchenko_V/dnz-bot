import argparse
import logging

from telegram.ext import Updater


from views import handlers
from data import update_groups_table, update_kg_table

logger = logging.getLogger(__name__)


def configure_logging():
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger('')
    logger.setLevel(level=logging.DEBUG)
    return logger


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main(token, update_groups, public_ip=None, ssl_cert_path=None, ssl_key_path=None,
         webhook=False, port=8443):
    logger = configure_logging()
    logger.info('Bot starting...')

    updater = Updater(token=token, use_context=True)

    dp = updater.dispatcher

    for h in handlers:
        dp.add_handler(h)
    dp.add_error_handler(error)

    update_kg_table()

    jq = updater.job_queue
    jq.run_repeating(update_groups, name='update_groups_table',
                     interval=60 * 60 * 12, first=0)  # repeat every 12 hours

    # Start the Bot
    if not webhook:
        updater.start_polling()
    else:
        updater.start_webhook(listen=public_ip,
                              port=port,
                              url_path=token,
                              key=ssl_key_path,
                              cert=ssl_cert_path,
                              webhook_url=f'https://{public_ip}:{port}/{token}')
        updater.idle()


def groups_table_updater(args):
    def callback(ctx):
        return update_groups_table(
            xlsx_url=args.groups_table_xlsx_url,
            csv_url=args.groups_table_csv_url,
            path=args.groups_table_path
        )

    return callback


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Lutsk Kindergarten bot')
    parser.add_argument('--public_ip',
                        help='This server\'s public ip')
    parser.add_argument('--ssl_cert',
                        help='SSL certificate path')
    parser.add_argument('--ssl_key',
                        help='SSL private key path')
    parser.add_argument('--webhook',
                        default=False,
                        action='store_true')
    parser.add_argument('--port',
                        default=8443,
                        help='Port to listen for updates')
    parser.add_argument('--token',
                        help='bot token')
    parser.add_argument('--groups_table_path',
                        help='A .csv file with current kindergarten groups data')
    parser.add_argument('--groups_table_csv_url',
                        help='An URL to the .csv file with current kindergarten groups data')
    parser.add_argument('--groups_table_xlsx_url',
                        help='An URL to the .xlsx file with current kindergarten groups data')
    args = parser.parse_args()
    main(update_groups=groups_table_updater(args),
         webhook=args.webhook, port=args.port,
         token=args.token, public_ip=args.public_ip,
         ssl_cert_path=args.ssl_cert, ssl_key_path=args.ssl_key)
